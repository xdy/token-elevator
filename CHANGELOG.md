## 1.2.0
- Added setting for base elevation level, and elevation change values.
- Changed default keybind modifier for Reset to ALT+CTRL+SHIFT.
- Changed names of several variables.
- Corrected keybind setting text for Reset.

## 1.1.2
- Cleared release file mess.

## 1.1.1
- Minor changes in manifest and readme.

## 1.1.0
- Merged clay.sweetser pull.
- Switched formatting rules to Prettier.
- Small manual edits to formatting.
- Changed manifest "compatibleCoreVersion" to broad v9 instead of build version.
- Updated readme to only include link to license instead of full license text.
